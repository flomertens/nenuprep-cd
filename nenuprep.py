#!/usr/bin/env python3
#
# nenuprep: NenuFAR pre-processing pipeline
# Author: Florent Mertens
#

__version__ = 0.4

import os
import re
import sys
import glob
import getpass
import smtplib
import logging
import fnmatch
import datetime
import logging.handlers

import click

import numpy as np

from libpipe import worker

import utils

# Setup logger
logging.getLogger().handlers = []
logger = logging.getLogger('nenuprep')
logger.setLevel('INFO')
logger.addHandler(logging.StreamHandler())

LOG_EMAIL_FROM = 'nenufarobs@obs-nancay.fr'


def get_subbands(nodes, dir):
    res = {}
    for node in nodes:
        res[node] = [f'{dir}/{sb}' for sb in fnmatch.filter(utils.ssh_listdir(node, dir), 'SB*.MS')]

    return res


def get_subbands_size(nodes, dir):
    res = {}
    for node in nodes:
        res[node] = utils.ssh_sizedir(node, f'{dir}/SB*.MS')

    return res


def get_all_tasks():
    d = {}
    for klass in AbstractTask.__subclasses__():
        if klass.name:
            d[klass.name] = klass
    return d


def get_all_tasks_descriptions():
    d = {}
    for klass in AbstractTask.__subclasses__():
        if klass.name:
            d[klass.name] = klass.desc
    return d


def get_task_host(task):
    if task.process is not None:
        return task.process.client.host
    return 'localhost'


class BufferingSMTPHandler(logging.handlers.BufferingHandler):
    ''' Buffered SMTP logging handler adapted from https://github.com/mikecharles/python-buffering-smtp-handler '''

    def __init__(self, mailhost, fromaddr, toaddrs, subject, capacity):
        logging.handlers.BufferingHandler.__init__(self, capacity)
        self.mailhost = mailhost
        self.mailport = None
        self.fromaddr = fromaddr
        self.toaddrs = toaddrs
        self.subject = subject

    def flush(self):
        if len(self.buffer) > 0:
            try:
                port = self.mailport
                if not port:
                    port = smtplib.SMTP_PORT
                smtp = smtplib.SMTP(self.mailhost, port)
                if isinstance(self.toaddrs, (tuple, list)):  # If to addrs is a list, then join them as a string
                    toaddrs = ','.join(self.toaddrs)
                else:
                    toaddrs = self.toaddrs
                msg = "From: {}\r\nTo: {}\r\nSubject: {}\r\n\r\n".format(self.fromaddr, toaddrs,
                                                                         self.subject)
                for record in self.buffer:
                    s = self.format(record)
                    msg = msg + s + "\r\n"
                smtp.sendmail(self.fromaddr, self.toaddrs, msg)
                smtp.quit()
            except:
                self.handleError(None)  # no particular record
            self.buffer = []


class AbstractTask(object):

    name = ''
    desc = ''
    do_not_run_if_previous_failled = False
    check_other_process_running = False
    confirmation_required = False

    def __init__(self, nodes_string, max_concurrent, dry_run, env_source_file=None, force_confirmation=False):
        self.max_concurrent = max_concurrent
        self.dry_run = dry_run
        self.w_pool = None
        self.nodes = worker.get_hosts(nodes_string)
        if not worker.localhost_shortname in self.nodes:
            self.nodes.append(worker.localhost_shortname)
        self.timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
        self.pid_files = []
        self.env_source_file = env_source_file
        self.force_confirmation = force_confirmation

    def has_worker_pool(self):
        return self.w_pool is not None

    def get_worker_pool(self):
        if self.w_pool is None:
            self.w_pool = worker.WorkerPool(self.nodes, name=self.name, max_tasks_per_worker=self.max_concurrent,
                                            debug=self.dry_run, dry_run=self.dry_run, 
                                            env_source_file=self.env_source_file)
        return self.w_pool

    def add_all(self, observations):
        for obs in observations:
            if self.check_other_process_running:
                pid_file = utils.PidFile(f'{self.get_run_dir(obs)}/nenuprep.pid')
                if pid_file.exists() and pid_file.is_running():
                    logger.error(f'Pre-processing is already running on observation {obs}.')
                    continue
                pid_file.create()
                self.pid_files.append(pid_file)
            if self.confirmation_required and not self.force_confirmation:
                if not click.confirm(click.style(f'Do you want to run task {self.name} on observation {obs.obs_id} ?',
                                                 fg='yellow')):
                    logger.info(f'Skip {obs.obs_id}')
                    continue

            self.add(obs)

    def get_run_dir(self, obs):
        run_dir = f'{obs.obs_dir}/run'
        if not os.path.exists(run_dir):
            os.makedirs(run_dir)

        return run_dir

    def get_log_filepath(self, obs, log_name, level='L1'):
        log_dir = f'{obs.obs_dir}/{level}/logs/{self.name}_{self.timestamp}'
        log_file = f'{log_dir}/{log_name}'
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        return log_file

    def add(self, obs):
        pass

    def execute(self):
        tasks_success, tasks_error =  self.get_worker_pool().execute()
        tasks_non_zero = self.get_non_zero_returncode_tasks(tasks_success)

        return tasks_success, tasks_error, tasks_non_zero

    def done(self):
        for pid_file in self.pid_files:
            pid_file.remove()

    def get_non_zero_returncode_tasks(self, tasks):
        return [task for task in tasks if task.returncode != 0]


class Process(AbstractTask):

    name = 'process'
    desc = 'Process the L0'
    check_other_process_running = True

    dppp_base_config_file = f'{utils.CONFIG_DIR}/dppp_process_l0.parset'

    def build_command(self, msin, msout, parameters):
        return f'DPPP {self.dppp_base_config_file} msin={msin} msout={msout} {" ".join(parameters)}'

    def add(self, obs):
        subbands = get_subbands(self.nodes, f'{obs.obs_dir}/L0')
        subbands_l1 = get_subbands(self.nodes, f'{obs.obs_dir}/L1')
        s_process = obs.settings.process

        for node, msins in subbands.items():
            utils.ssh_mkdir(node, f'{obs.obs_dir}/L1')
            for msin in msins:
                msout = msin.replace('/L0', '/L1')
                subband = os.path.basename(msin).replace('.MS', '')
                pid_file = utils.PidFile(f'{self.get_run_dir(obs)}/{self.name}_{subband}.pid')

                if msout in subbands_l1[node] and not pid_file.exists():
                    logger.info(f'{node}:{msout} already processed. skip it.')
                    continue

                if pid_file.exists() and pid_file.is_running():
                    logger.info(f'{node}:{msout} currently being processed elsewhere. skip it.')
                    continue

                storage_manager = 'dysco' if s_process.compress else ''

                parameters = [f'msin.startchan={s_process.startchan}',
                              f'msin.nchan={s_process.nchan}',
                              f'avg.type=average',
                              f'avg.freqstep={s_process.avg_freqstep}',
                              f'avg.timestep={s_process.avg_timestep}',
                              f'msout.storagemanager={storage_manager}']

                if s_process.flag_rfi:
                    flag_strategy = os.path.join(s_process.flag_config_dir, s_process.flag_strategy)
                    parameters.append(f'flag.type=aoflagger')
                    parameters.append(f'flag.memoryperc={s_process.flag_memoryperc}')
                    parameters.append(f'flag.strategy={flag_strategy}')
                    parameters.append(f'steps=[flag,avg]')
                else:
                    parameters.append(f'steps=[avg]')

                cmd = self.build_command(msin, msout, parameters)
                log_file = self.get_log_filepath(obs, f'{subband}.log')
                self.get_worker_pool().add(cmd, name=subband, output_file=log_file, run_on_host=node,
                                           pid_filename=pid_file.name, keep_pid_file_on_error=True)


class Rsync(AbstractTask):

    name = 'rsync'
    desc = 'Transfert the L1 to databf'
    check_other_process_running = True

    def add(self, obs):
        s_rsync = obs.settings.rsync
        in_dir = f'{obs.obs_dir}/L1'
        out_dir = in_dir.replace(obs.settings.data_dir, s_rsync.target_data_dir)

        databf_group = f'nfr{obs.code}admin'
        cmd = f'rsync --progress -v -am --groupmap *:{databf_group} -r {in_dir}/ {s_rsync.target_username}@{s_rsync.target_hostname}:{out_dir}/'
        utils.ssh_mkdir(s_rsync.target_hostname, out_dir, s_rsync.target_username)
        for node in self.nodes:
            log_file = self.get_log_filepath(obs, f'{node}.log')
            self.get_worker_pool().add(cmd, output_file=log_file, run_on_host=node)


class RmL0(AbstractTask):

    name = 'rm_l0'
    do_not_run_if_previous_failled = True
    desc = 'Remove the L0'
    check_other_process_running = True
    confirmation_required = True

    def add(self, obs):
        l0_dir = f'{obs.obs_dir}/L0'
        cmd = f'rm -rvf {l0_dir}/SB*.MS'
        for node in self.nodes:
            self.get_worker_pool().add(cmd, output_file=self.get_log_filepath(obs, f'{node}.log'), run_on_host=node)


class RmL1(AbstractTask):

    name = 'rm_l1'
    do_not_run_if_previous_failled = True
    desc = 'Remove the L1'
    check_other_process_running = True
    confirmation_required = True

    def add(self, obs):
        l1_dir = f'{obs.obs_dir}/L1'
        cmd = f'rm -rvf {l1_dir}/SB*.MS'
        for node in self.nodes:
            self.get_worker_pool().add(cmd, output_file=self.get_log_filepath(obs, f'{node}.log'), run_on_host=node)


class ListSubbandL0(AbstractTask):

    name = 'list_l0'
    level_dir = 'L0'
    desc = 'List the L0 subbands'

    def add(self, obs):
        subbands = get_subbands(self.nodes, f'{obs.obs_dir}/{self.level_dir}')
        logger.info(f'{obs.obs_dir}:')
        for node, msins in subbands.items():
            for msin in msins:
                logger.info(f'{node}: {os.path.basename(msin)}')


class ListSubbandL1(ListSubbandL0, AbstractTask):

    name = 'list_l1'
    level_dir = 'L1'
    desc = 'List the L1 subbands'


class CountSubbandL0(AbstractTask):

    name = 'count_l0'
    level_dir = 'L0'
    desc = 'Count the L0 subbands for each nodes'

    def add(self, obs):
        subbands = get_subbands(self.nodes, f'{obs.obs_dir}/{self.level_dir}')
        logger.info(f'{obs.obs_dir}:')
        for node, msins in subbands.items():
            logger.info(f'{node}: {len(msins)}')


class CountSubbandL1(CountSubbandL0, AbstractTask):

    name = 'count_l1'
    level_dir = 'L1'
    desc = 'Count the L1 subbands for each nodes'


class DuSubbandL0(AbstractTask):

    name = 'du_l0'
    level_dir = 'L0'
    desc = 'Get the size of the L0 subbands'

    def add(self, obs):
        nodes_size = get_subbands_size(self.nodes, f'{obs.obs_dir}/{self.level_dir}')
        logger.info(f'{obs.obs_dir}:')
        for node, subbands_size in nodes_size.items():
            for subband, size in subbands_size.items():
                logger.info(f'{node} {os.path.basename(subband)}: {size}')


class DuSubbandL1(DuSubbandL0, AbstractTask):

    name = 'du_l1'
    level_dir = 'L1'
    desc = 'Get the size of the L1 subbands'


class GenerateQualityPlots(AbstractTask):

    name = 'quality'
    desc = 'Generate quality plots from L1. You need to run rsync_quality first !'
    check_other_process_running = True

    pol_nums = {'XX': 0, 'XY': 1, 'YX': 2, 'YY': 3}

    def build_command(self, msins, stat_name, pol_name, sw_name, res_dir, log_scale):
        log_scale_str = ''
        if log_scale:
            log_scale_str = '--log'
        pol_num = self.pol_nums[pol_name]
        return f"aostats plot {' '.join(msins)} -o {res_dir}/{sw_name} --name {sw_name} -p {pol_num} {stat_name} {log_scale_str}"

    def add(self, obs):
        all_sb = np.sort(np.array(glob.glob(f'{obs.obs_dir}/L1_QUALITY/SB*.MS')))
        all_sb_num = np.array([int(os.path.basename(f)[2:5]) for f in all_sb]).astype(np.int)
        res_dir = f'{obs.obs_dir}/L1/diagnostics'
        if not os.path.exists(res_dir):
            os.makedirs(res_dir)
        if len(all_sb_num) > 0:
            for sw_sbs in obs.settings.quality.sws:
                sw_name, sb_start, sb_end = sw_sbs.split('-')
                msins = all_sb[(all_sb_num >= int(sb_start)) & (all_sb_num <= int(sb_end))]

                if len(msins) > 0:
                    for stat_pol in obs.settings.quality.stat_pols:
                        stat_name, pol_name = stat_pol.split('_')
                        pol_name = pol_name.upper()

                        log_scale = stat_name in obs.settings.quality.log_scales

                        cmd = self.build_command(msins, stat_name, pol_name, sw_name, res_dir, log_scale)

                        log_file = self.get_log_filepath(obs, f'quality_{stat_name}_{pol_name}_{sw_name}.log')
                        self.get_worker_pool().add(cmd, output_file=log_file, run_on_host=worker.localhost_shortname)


class RsyncQualityTables(AbstractTask):

    name = 'rsync_quality'
    desc = 'Transfert all QUALITY tables to localhost and store them in L1_QUALITY'
    check_other_process_running = True

    def __init__(self, nodes_string, max_concurrent, dry_run, env_source_file=None, force_confirmation=False):
        AbstractTask.__init__(self, nodes_string, 1, dry_run, env_source_file=env_source_file, 
                              force_confirmation=force_confirmation)

    def add(self, obs):
        l1_dir = f'{obs.obs_dir}/L1'
        outdir = l1_dir.replace('L1', 'L1_QUALITY')
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        for node in self.nodes:
            rsync_filter = '--include="*/*/table*" --include="SB*.MS/ANTENNA" --include="SB*.MS/QUALITY*" --exclude="*/*"'
            node_str = f'{node}:' if node is not worker.localhost_shortname else ''
            cmd = f'rsync --progress -v -am -r {rsync_filter} {node_str}{l1_dir}/ {outdir}'
            log_file = self.get_log_filepath(obs, f'{node}.log')
            self.get_worker_pool().add(cmd, output_file=log_file, run_on_host=worker.localhost_shortname)


tasks_help = '\n'.join([f'  {name.ljust(17)}{desc}' for name, desc in get_all_tasks_descriptions().items()])


@click.command(epilog='\b\nAvailable tasks:\n' + tasks_help)
@click.version_option(__version__)
@click.argument('config_file', type=click.Path(exists=True, dir_okay=False))
@click.argument('obs_parset_or_dirs', type=click.Path(exists=True), nargs=-1)
@click.option('--force', '-f', help='Force confirmation (for products removal tasks)', is_flag=True)
@click.option('--dry_run', '-d', help='Dry run, only print commands', is_flag=True)
def main(config_file, obs_parset_or_dirs, force, dry_run):
    ''' NenuFAR pre-processing pipeline

        \b
        config_file: Nenuprep configuration file
        OBS_DIR: Observation parset or observation directory

        Parameters of the configuration file (in TOML format):

        \b
        tasks: list of tasks (see Available tasks below).
        data_dir: Base data directory (default: /data_NRI/nenufar-nri/).
        log_dir: Directory where the script logs are stored (default: /home/nenufarobs/pre_processing/logs).
        log_email: list of email to send the script output when completed.
        worker.max_concurrent: number of concurrent process on a single node (default: 2).
        worker.nodes_string: List of nodes on which to run processing, coma separated with support for numeric range expansion (default: copper1-4)
        worker.env_file: Shell environment file to be source before starting the task to e.g. set-up PATH, etc...
        process.avg_timestep: Number of time slots to average (default: 4).
        process.avg_freqstep: Number of channels to average (default: 5).
        process.startchan: First channel to use from the input MS (default: 0)
        process.nchan: Number of channels to use from the input MS (0 means till the end) (default: 0)
        process.compress: Use dysco to compress the resulting MS (default: false)
        process.flag_strategy: AOflagger strategy file (if not absolute path, it will check process.flag_config_dir) (default: Nenufar-64C1S.rfis)
        process.flag_config_dir: AOflagger strategy file directory (default: /home/nenufarobs/pre_processing/config_files)
        quality.sws: List of range of subbands on which to plots QUALITY statistics. Format is ['<name>-<start_num_sb>-<end_num_sb>', ...] (default: ['SW01-50-150', 'SW02-150-250', 'SW03-250-350', 'SW04-350-450']).
        quality.stat_pols: List of statistics to plots (default: ['SNR_XX', 'SNR_YY', 'RFIPercentage_XX']).
        rsync.target_data_dir: Base data directory on databf (default: /data/nenufar-nri/).
        rsync.target_hostname: Name of the target server (default: databfnfrdt).
        rsync.target_username: User to use for rsync (default: nfrobs)

    '''
    settings = utils.Settings.load_with_defaults(config_file)
    tasks = settings.tasks

    # Setup file logger
    if not os.path.exists(settings.log_dir):
        os.makedirs(settings.log_dir)

    file_log_handler = logging.FileHandler(f"{settings.log_dir}/nenuprep_{datetime.datetime.now().strftime('%Y_%m_%d')}.log")
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_log_handler.setFormatter(formatter)
    logger.addHandler(file_log_handler)

    if settings.log_email:
        smtp_handler = BufferingSMTPHandler('localhost', LOG_EMAIL_FROM, settings.log_email, 
                                            f'NenuFAR pre-processing completed: {", ".join(tasks)}', 500)
        logger.addHandler(smtp_handler)

    observations = utils.get_observations_from_parset_or_dirs(obs_parset_or_dirs, settings=settings)

    if not len(observations):
        logger.info('No observations to process')
        sys.exit(0)

    # check that all given tasks are correct
    all_tasks = get_all_tasks()

    for task_name in tasks:
        if task_name not in all_tasks:
            logger.error(f'Error: task {task_name} does not exist.')
            sys.exit(1)

    logger.info(f'Start pre-processing at {datetime.datetime.now().strftime("%Y_%m_%d %H:%M:%S")} by user {getpass.getuser()}\n')

    # start running tasks
    task_results = dict()
    one_task_failled = False

    logger.info(f'Tasks to run: {", ".join(tasks)}')
    logger.info(f'Observations to process: {", ".join(map(str, observations))}')

    for task_name in tasks:
        logger.info('')
        logger.info(f'Start task {task_name} at {datetime.datetime.now().strftime("%Y_%m_%d %H:%M:%S")}')
        if one_task_failled and all_tasks[task_name].do_not_run_if_previous_failled:
            logger.warning(f'  An earlier task failed, skipping {task_name}')
            continue

        task = all_tasks[task_name](settings.worker.nodes_string, settings.worker.max_concurrent, dry_run, 
                                    env_source_file=settings.worker.env_file, force_confirmation=force)
        task.add_all(observations)
        
        if task.has_worker_pool() and len(task.get_worker_pool().tasks) > 0:
            subtasks_success, subtasks_error, subtasks_non_zero = task.execute()
            task_results[task_name] = subtasks_success, subtasks_error, subtasks_non_zero

            if len(subtasks_error) > 0 or len(subtasks_non_zero) > 0:
                logger.warning('  At least one sub-task failed')
                one_task_failled = True

        task.done()

    logger.info('')
    logger.info(f'All done at {datetime.datetime.now().strftime("%Y_%m_%d %H:%M:%S")}')

    return_code = 0

    # check tasks results
    if task_results:
        for task_name, (subtasks_success, subtasks_error, subtasks_non_zero) in task_results.items():
            n_c = len(subtasks_success)
            n_i = len(subtasks_error)
            n_e = len(subtasks_non_zero)
            if n_i > 0 or n_e > 0:
                return_code = 1
            logger.info(f'Task {task_name}: Completed: {n_c} Not completed: {n_i} Completed with error: {n_e}')
            if n_i > 0:
                logger.error('Not completed:')
                for task in subtasks_error:
                    logger.error(f'  Task {task.name} on node {get_task_host(task)}')
                    if task.output_file is not None:
                        logger.error(f'  -> check log file: {task.output_file}')
            if n_e > 0:
                logger.error(' Completed with error:')
                for task in subtasks_non_zero:
                    logger.error(f'  Task {task.name} on node {get_task_host(task)}')
                    if task.output_file is not None:
                        logger.error(f'  -> check log file: {task.output_file}')

    logger.info('')
    logger.info(f'End pre-processing at {datetime.datetime.now().strftime("%Y_%m_%d %H:%M:%S")} with return code {return_code}')

    sys.exit(return_code)


if __name__ == '__main__':
    main()
