BASE_DIR=~/soft/DP3-5.2

export LD_LIBRARY_PATH=/opt/shared/LofarStMan/lib:/opt/shared/dysco/lib:${BASE_DIR}/EveryBeam/build/lib/:${BASE_DIR}/DP3/build/lib:${BASE_DIR}/aoflagger/build/lib:${LD_LIBRARY_PATH}
export PATH=${BASE_DIR}/DP3/build/bin:${BASE_DIR}/aoflagger/build/bin:~/.local/bin/:${PATH}
