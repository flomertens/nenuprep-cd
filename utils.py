import os
import re
import sys
from enum import Enum
import logging
import subprocess

from libpipe import worker
from libpipe.settings import BaseSettings

from nenupy.observation import parset

import utils

logger = logging.getLogger('nenuprep')

HOME_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_DIR = f'{HOME_DIR}/config_files'

DEFAULT_CONFIG_FILE = os.path.join(CONFIG_DIR, 'default_pipeline.toml')


class ProcStatus(Enum):
    RUNNING = 1
    STOPPED = 2
    DEAD = 3


def ssh_cmd(node, cmd_list, shell=False, **kargs):
    if shell:
        cmd_list = ' '.join(cmd_list)
        if node != worker.localhost_shortname:
            cmd_list = f"ssh {node} '{cmd_list}'"
    else:
        if node != worker.localhost_shortname:
            cmd_list = ['ssh', node] + cmd_list
    p = subprocess.run(cmd_list, shell=shell, **kargs)
    return p


def ssh_sizedir(node, dir):
    r = ssh_cmd(node, [f"du -hs {dir}"], stdout=subprocess.PIPE, universal_newlines=True, shell=True).stdout.split()
    return dict(zip(r[1::2], r[::2]))


def ssh_mkdir(node, dir, user=None):
    if user is not None:
        node = f'{user}@{node}'
    ssh_cmd(node, ["mkdir", "-p",  f"{dir}"])


def ssh_listdir(node, dir):
    return ssh_cmd(node, ["ls", f"{dir}"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, universal_newlines=True).stdout.splitlines()


def ssh_ps_status(node, pid):
    return ssh_cmd(node, ["ps", f"-h -o s -p {pid}"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, universal_newlines=True, shell=True).stdout.strip()


def get_observations_from_parset_or_dirs(obs_parset_or_dirs, settings):
    observations = []
    for obs_parset_or_dir in obs_parset_or_dirs:
        if os.path.isfile(obs_parset_or_dir):
            # we need to initiate a new settings object as the it may be altered from the default one based on the parset
            obs = utils.Observation.from_obs_parset(obs_parset_or_dir, settings.copy())
        else:
            obs_dir = os.path.abspath(obs_parset_or_dir)
            # check that the obs_dir follow the pattern
            obs_dir_path_pattern = os.path.join(settings.data_dir, '[a-zA-Z0-9]+', '[0-9]{4}', '[0-9]{2}', '.*')

            if not re.match(obs_dir_path_pattern, obs_dir):
                logger.error(f'Error: {obs_dir} is not a correct observation directory')
                sys.exit(1)
            # here we use the default settings unaltered
            obs = utils.Observation(obs_dir, settings)
        observations.append(obs)
    return observations


class Settings(BaseSettings):

    DEFAULT_SETTINGS = DEFAULT_CONFIG_FILE

    def __init__(self, file, d):
        BaseSettings.__init__(self, file, d)


class PidFile(object):

    def __init__(self, name):
        self.name = name

    def create(self):
        with open(self.name, 'w') as fd:
            fd.write(f'{worker.localhost_shortname} {os.getpid()}')

    def exists(self):
        return os.path.exists(self.name)

    def get_host_pid(self):
        with open(self.name) as fd:
            s = fd.read()
        try:
            host, pid = s.split()
            return host, pid
        except Exception:
            print(f'Error: can not retrieve PID from {self.name}')
        return None, 0

    def is_running(self):
        return self.get_status() is not ProcStatus.DEAD

    def get_status(self):
        try:
            host, pid = self.get_host_pid()
            status = ssh_ps_status(host, int(pid))
            if 'D' in status or 'R' in status or 'S' in status:
                return ProcStatus.RUNNING
            elif 'T' in status.upper():
                return ProcStatus.STOPPED
            return ProcStatus.DEAD
        except Exception:
            return ProcStatus.DEAD

    def remove(self):
        os.remove(self.name)


class Observation(object):

    def __init__(self, obs_dir, settings):
        self.obs_dir = os.path.abspath(obs_dir)
        self.obs_id = os.path.basename(self.obs_dir)
        self.code = self.obs_dir.split('/')[-4]
        self.settings = settings

    def __str__(self):
        return self.obs_id

    @staticmethod
    def from_obs_parset(obs_parset, settings):
        p = parset.Parset(parset=obs_parset)
        obs_id = os.path.basename(obs_parset).split('.')[0]
        obs_code = p.observation['topic'][:4]
        year = obs_id[:4]
        month = obs_id[4:6]
        obs_dir = f'{settings.data_dir}/{obs_code}/{year}/{month}/{obs_id}'

        # adjust settings based on the parset here

        return Observation(obs_dir, settings)

