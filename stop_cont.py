#!/usr/bin/env python3

import os
import sys
import glob
import click
import logging
import subprocess

from nenupy import observation

import utils

# Setup logger
logging.getLogger().handlers = []
logger = logging.getLogger('nenuprep')
logger.setLevel('INFO')
logger.addHandler(logging.StreamHandler())

# Set here the path of the stop/start process script
DEFAULT_STOP_START_PROCESS = "/home/nenufarobs/stop_start/stop_start_process.py"


def kill_remote(pid_file):
    host, pid = pid_file.get_host_pid()
    os.system(f'ssh {host} "kill -TERM -{pid}"')


def ssh_start_stop(pid_file, command, script):
    host, pid = pid_file.get_host_pid()
    ret = utils.ssh_cmd(host, [script, command, f'-p {pid}']).returncode

    if ret != 0:
        print(f'Warning: sending {command} on {obs} returned {ret}.')

    return ret


def ssh_get_process_group_info(host, pid):
    return utils.ssh_cmd(host, [f'ps --forest -o pid,pgid,%cpu,%mem,stat,cmd,etime -p $(pgrep -g $(ps -h -o pgid -p {pid}))'], 
                         stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, universal_newlines=True, shell=True).stdout


def status_string(pid_file):
    status = pid_file.get_status()
    if status == utils.ProcStatus.RUNNING:
        return click.style('Running', fg='green')
    elif status == utils.ProcStatus.STOPPED:
        return click.style('Stopped', fg='yellow')
    return click.style('Not running', fg='red')


@click.command()
@click.argument('command', type=click.Choice(['stop', 'cont', 'status', 'killXX']))
@click.argument('obs_parset_or_dirs', type=click.Path(exists=True), nargs=-1)
@click.option('--master_pid_filename', help='Name of the master PID filename', type=str, default='nenuprep.pid')
@click.option('--start_stop_script', help='Path of the start/stop script', type=str, default=DEFAULT_STOP_START_PROCESS)
@click.option('--verbose', '-v', help='Verbose output', is_flag=True)
def main(obs_parset_or_dirs, command, master_pid_filename, start_stop_script, verbose=True):
    ''' Check and set NenuFAR pre-processing status

        \b
        COMMAND: Command can be either 'stop', 'cont' or 'status'
        OBS_PARSET_OR_DIRS: Observation parset or observation directory
        
        'stop' command: For each obs_parset_or_dirs, we look for the master script running pre-processing on this observation
        and all its child process running on different nodes. The master script is first stopped (SIGSTOP) followed by all its children.

        'cont' command: For each obs_parset_or_dirs, we look for the master script running pre-processing on this observation
        and all its child process running on different nodes. The child processes are first restarted, followed by the master.

        'status' command: Check for the status of pre-processing processes.
    '''

    settings = utils.Settings.get_defaults()

    if len(obs_parset_or_dirs) == 0:
        all_master_pid = glob.glob(os.path.join(settings.data_dir, 'ES[0-9][0-9]', '[0-9][0-9][0-9][0-9]',
                                                '[0-9][0-9]', '*', 'run', master_pid_filename))
        obs_parset_or_dirs = [os.path.dirname(os.path.dirname(k)) for k in all_master_pid]

    observations = utils.get_observations_from_parset_or_dirs(obs_parset_or_dirs, settings)

    all_ret = 0

    for obs in observations:
        master_pid_file = utils.PidFile(os.path.join(obs.obs_dir, 'run', master_pid_filename))
        child_pid_files = [utils.PidFile(k) for k in glob.glob(os.path.join(obs.obs_dir, 'run', '*.pid')) if k is not master_pid_file.name]

        if command == 'status':
            print(f'{obs.obs_dir} status: {status_string(master_pid_file)}')
            for child_pid_file in child_pid_files:
                host, pid = child_pid_file.get_host_pid()
                print(f'| Process group of {pid} on {click.style(host, fg="cyan")} status: {status_string(child_pid_file)}')
                if verbose:
                    print(f'PID file: {child_pid_file.name}')
                    print(ssh_get_process_group_info(host, pid))
            continue

        if command == 'killXX':
            for pid_file in child_pid_files:
                kill_remote(pid_file)
            kill_remote(master_pid_file)
            continue

        # If master pid file does not exist, skip
        if not master_pid_file.exists():
            continue

        # First stop the master
        if command == 'stop':
            ret = ssh_start_stop(master_pid_file, command, start_stop_script)
            all_ret = all_ret | ret

        # stop or cont the child processes
        for pid_file in child_pid_files:
            ret = ssh_start_stop(pid_file, command, start_stop_script)
            all_ret = all_ret | ret

        # Last cont the master
        if command == 'cont':
            ret = ssh_start_stop(master_pid_file, command, start_stop_script)
            all_ret = all_ret | ret

    sys.exit(all_ret)


if __name__ == '__main__':
    main()
