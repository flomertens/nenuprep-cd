# Change Log

# 0.4 - 2022-06-02

- [Fix] Typo on DPPP parameter name flag.memoryperc

## 0.3 - 2022-06-01

- [New] Return non zero return code when any tasks finished with an error 
- [New] Add the process.flag_rfi parameters to enable/disable RFI flagging
- [New] Add process.flag_memoryperc parameter

## 0.2  - 2021-02-24

- Numerous improvements\ 

## 0.1 - 2020-09-02

- Inital commit
