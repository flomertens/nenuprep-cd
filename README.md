NenuFAR Pre-processing Pipeline (nenuprep)
========================================

The role of the pre-processing pipeline is to:

    Flag RFI at the highest time and frequency resolution (L0 data).
    Average the data at a lower time and frequency resolution and possibly compress it to reduce its disk footprint (L1 data).
    Produce diagnostics plots.
    Copy the produced L1 data to the data archive server

Additionally the pipeline include a tool to pause and resume any processing started by the pipeline.

For installation instructions and documentation, check the confluence page https://confluence2.obs-nancay.fr/pages/viewpage.action?pageId=119671339
